FROM eu.gcr.io/dataans/auth_builder:4 as builder

COPY ./Cargo.toml ./Cargo.toml
RUN rm src/*.rs

ADD src ./src

RUN rm ./target/release/deps/auth*
RUN cargo build --release

FROM frolvlad/alpine-glibc:glibc-2.29
ARG APP=/usr/src/app

EXPOSE 8000

COPY --from=builder /auth/target/release/auth ./auth

CMD ["./auth"]
