#!/bin/bash

# acquire database initialization scipt
if test -f "$FILE"; then
    curl "https://gitlab.com/dataans/infrastructure/-/raw/main/db/schema.sql" > init.sql
    echo "init.sql acquired!"
else
    echo "init.sql already exist"
fi

# set up database credentials
export DB_NAME=dataans
export DB_USER=postgres
export DB_PASS=postgres

# set up testing database
docker-compose up -d

# check that database is running
DB_CONTAINER_ID=$(docker ps | grep "postgres" | cut -d ' ' -f 1)
echo $DB_CONTAINER_ID

if [ -z "$DB_CONTAINER_ID" ]
then
    echo "Failed to create a database container."
    exit 1
else
    echo "Database container has been created."
fi

# todo: replace sleep with while loop
sleep 8

docker ps

psql "postgres://${DB_USER}:${DB_PASS}@127.0.0.1:5432/${DB_NAME}" -c "select * from users;"

export PORT=8080
export AVATARS_BUCKET=avatars.dataans.com
export CIPHER_KEY=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
export SESSION_ENCRYPTION_KEY=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
export REDIS_URL=redis://127.0.0.1
export GITHUB_CLIENT_ID=1
export GITHUB_CLIENT_SECRET=2
export GITHUB_REDIRECT_URI=http://localhost:8081

export LOG_LEVEL=debug

cargo test

psql "postgres://${DB_USER}:${DB_PASS}@127.0.0.1:5432/${DB_NAME}" -c "select * from users;" > psql.logs.txt

# stop and delete the database container
docker kill $DB_CONTAINER_ID
docker rm -f $DB_CONTAINER_ID
echo "Database has been removed!"
