use std::borrow::Cow;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct ApiError<'a, 'b> {
    pub message: Cow<'a, str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub details: Option<Cow<'b, str>>,
}

pub const BAD_CREDENTIALS: ApiError<'static, 'static> = ApiError {
    message: Cow::Borrowed("Bad credentials."),
    details: None,
};
pub const INTERNAL: ApiError<'static, 'static> = ApiError {
    message: Cow::Borrowed("Internal error."),
    details: None,
};
pub const USER_ALREADY_EXIST: ApiError<'static, 'static> = ApiError {
    message: Cow::Borrowed("User already exist."),
    details: None,
};
