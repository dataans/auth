use std::borrow::Cow;

use actix_web::cookie::Cookie;
use actix_web::{delete, get, post, put, web, HttpRequest, HttpResponse, Responder};
use reqwest::StatusCode;
use session_manager::model::Session;

use super::errors::ApiError;
use super::types::SetPrimaryEmailRequest;
use crate::api::types::{
    AddEmailRequest, ChangePasswordRequest, Enable2FaRequest, OAuthRequest, RecoveryCodesRequest, RemoveEmailRequest,
    SignInRequest, SignUpRequest, TotpRequest,
};
use crate::app::AppData;
use crate::services::auth::{AuthError, SUB_SESSION_COOKIE_NAME};
use crate::services::oauth::OAuthError;

pub async fn default_route(_req: HttpRequest) -> impl Responder {
    web::Json(ApiError {
        message: Cow::Borrowed("Not found"),
        details: None,
    })
    .customize()
    .with_status(StatusCode::NOT_FOUND)
}

#[get("/")]
pub async fn root_health() -> impl Responder {
    HttpResponse::Ok().body("root: ok.")
}

#[get("/health")]
pub async fn health() -> impl Responder {
    HttpResponse::Ok().body("auth ok.")
}

#[post("/sign-in")]
pub async fn sign_in(
    req: HttpRequest,
    credentials: web::Json<SignInRequest>,
    app: web::Data<AppData>,
) -> Result<impl Responder, AuthError> {
    let token = app
        .auth_service
        .lock()
        .await
        .sign_in(&credentials, &mut *app.crypto_utils.lock().await)
        .await?;

    let mut response = "".customize().with_status(StatusCode::OK).respond_to(&req);
    response.add_cookie(&Cookie::new(token.token_type, token.token))?;

    Ok(response)
}

#[post("/2fa/totp")]
pub async fn verify_totp(
    code: web::Json<TotpRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> Result<impl Responder, AuthError> {
    let sub_session_cookie = req
        .cookie(SUB_SESSION_COOKIE_NAME)
        .ok_or_else(|| AuthError::NotAuthorized("is not authorized. No sub session cookie".into()))?;
    let token = app
        .auth_service
        .lock()
        .await
        .verify_totp(
            sub_session_cookie.value(),
            code.into_inner(),
            &mut *app.crypto_utils.lock().await,
        )
        .await?;

    let mut response = "".customize().with_status(StatusCode::OK).respond_to(&req);
    response.add_cookie(&Cookie::new(token.token_type, token.token))?;

    Ok(response)
}

#[post("/2fa")]
pub async fn set_2fa(
    data: web::Json<Enable2FaRequest>,
    app: web::Data<AppData>,
    session: Session,
) -> Result<impl Responder, AuthError> {
    Ok(web::Json(
        app.auth_service
            .lock()
            .await
            .enable_2fa(data.into_inner(), &session.user_id, &mut *app.crypto_utils.lock().await)
            .await?,
    ))
}

#[post("/2fa/recovery_code")]
pub async fn regenerate_2fa_recovery_codes(
    data: web::Json<RecoveryCodesRequest>,
    app: web::Data<AppData>,
    session: Session,
) -> Result<impl Responder, AuthError> {
    Ok(web::Json(
        app.auth_service
            .lock()
            .await
            .regenerate_2fa_recovery_codes(data.into_inner(), &session.user_id, &mut *app.crypto_utils.lock().await)
            .await?,
    ))
}

#[put("/password")]
pub async fn change_password(
    data: web::Json<ChangePasswordRequest>,
    app: web::Data<AppData>,
    session: Session,
) -> impl Responder {
    app.auth_service
        .lock()
        .await
        .change_password(data.into_inner(), &session.user_id, &mut *app.crypto_utils.lock().await)
        .await
        .map(|_| "")
}

#[put("/email")]
pub async fn set_primary_email(
    data: web::Json<SetPrimaryEmailRequest>,
    app: web::Data<AppData>,
    session: Session,
) -> Result<impl Responder, AuthError> {
    Ok(web::Json(
        app.auth_service
            .lock()
            .await
            .set_user_primary_email(data.into_inner(), &session.user_id, &mut *app.crypto_utils.lock().await)
            .await?,
    ))
}

#[post("/email")]
pub async fn add_email(
    data: web::Json<AddEmailRequest>,
    app: web::Data<AppData>,
    session: Session,
) -> Result<impl Responder, AuthError> {
    Ok(web::Json(
        app.auth_service
            .lock()
            .await
            .add_user_email(data.into_inner(), &session.user_id, &mut *app.crypto_utils.lock().await)
            .await?,
    ))
}

#[delete("/email")]
pub async fn delete_email(
    data: web::Json<RemoveEmailRequest>,
    app: web::Data<AppData>,
    session: Session,
) -> Result<impl Responder, AuthError> {
    Ok(web::Json(
        app.auth_service
            .lock()
            .await
            .delete_user_email(data.into_inner(), &session.user_id, &mut *app.crypto_utils.lock().await)
            .await?,
    ))
}

#[post("/sign-up")]
pub async fn sign_up(
    sign_up_request: web::Json<SignUpRequest>,
    app: web::Data<AppData>,
) -> Result<impl Responder, AuthError> {
    let response_data = app
        .auth_service
        .lock()
        .await
        .sign_up(&sign_up_request, &mut *app.crypto_utils.lock().await)
        .await?;

    Ok(web::Json(response_data).customize().with_status(StatusCode::CREATED))
}

#[post("/github")]
pub async fn github_oauth(
    req: HttpRequest,
    sign_up_request: web::Json<OAuthRequest>,
    app: web::Data<AppData>,
) -> Result<impl Responder, OAuthError> {
    let token = app.oauth_service.lock().await.github(&sign_up_request.code).await?;

    let mut response = "".customize().with_status(StatusCode::OK).respond_to(&req);
    response.add_cookie(&Cookie::new(token.token_type, token.token))?;

    Ok(response)
}

#[post("/google")]
pub async fn google_oauth(
    req: HttpRequest,
    sign_up_request: web::Json<OAuthRequest>,
    app: web::Data<AppData>,
) -> Result<impl Responder, OAuthError> {
    let token = app.oauth_service.lock().await.google(&sign_up_request.code).await?;

    let mut response = "".customize().with_status(StatusCode::OK).respond_to(&req);
    response.add_cookie(&Cookie::new(token.token_type, token.token))?;

    Ok(response)
}
