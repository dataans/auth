pub mod errors;
pub mod handlers;
pub mod types;

use actix_web::body::{BoxBody, MessageBody};
use actix_web::cookie::time::{Duration, OffsetDateTime};
use actix_web::cookie::Cookie;
use actix_web::http::StatusCode;
use actix_web::{HttpRequest, HttpResponse, Responder};

use crate::services::auth::DAYS_SESSION_VALID;

pub struct AuthResponse<T: MessageBody> {
    body: T,
    code: StatusCode,
    cookies: Vec<(String, String)>,
}

impl<T: MessageBody> AuthResponse<T> {
    pub fn new(code: StatusCode, body: T) -> Self {
        Self {
            body,
            code,
            cookies: Vec::new(),
        }
    }

    pub fn with_cookie(&mut self, name: String, value: String) {
        self.cookies.push((name, value));
    }
}

impl<T: MessageBody + 'static> Responder for AuthResponse<T> {
    type Body = BoxBody;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        let AuthResponse { code, cookies, body } = self;

        let mut response = HttpResponse::Ok();
        let response_builder = response.status(code);

        for (name, value) in cookies {
            response_builder.cookie(
                Cookie::build(name, value)
                    .http_only(true)
                    .path("/")
                    .expires(OffsetDateTime::now_utc().checked_add(Duration::days(DAYS_SESSION_VALID)))
                    .finish(),
            );
        }

        response_builder.body(body)
    }
}
