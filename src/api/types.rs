use serde::{Deserialize, Serialize};
use time::serde::rfc3339;
use time::OffsetDateTime;
use uuid::Uuid;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct SignUpRequest {
    pub username: String,
    pub email: String,
    #[serde(rename = "fullName")]
    pub full_name: String,
    pub password: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct SignInRequest {
    pub login: String,
    pub password: String,
}

#[derive(Deserialize, Debug)]
pub struct OAuthRequest {
    pub code: String,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct UserResponse {
    pub id: Uuid,
    pub username: String,
    pub full_name: String,
    pub emails: Vec<String>,
    pub primary_email: String,
    pub avatar_url: String,
    #[serde(with = "rfc3339")]
    pub joined_at: OffsetDateTime,
    pub is_2fa_enabled: bool,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SignInResponse {
    pub needs_2fa: bool,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SignUpResponse {
    pub user_id: Uuid,
}

#[derive(Serialize, Deserialize)]
pub struct EmailWithPassword {
    pub email: String,
    pub password: String,
}

pub type AddEmailRequest = EmailWithPassword;
pub type RemoveEmailRequest = EmailWithPassword;
pub type SetPrimaryEmailRequest = EmailWithPassword;

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ChangePasswordRequest {
    pub password: String,
    pub new_password: String,
}

#[derive(Serialize, Deserialize)]
pub struct TotpRequest {
    pub code: String,
}

#[derive(Serialize, Deserialize)]
pub struct Enable2FaRequest {
    pub enable: bool,
    pub password: String,
}

#[derive(Serialize, Deserialize)]
pub struct RecoveryCodesRequest {
    pub password: String,
}

#[derive(Serialize, Deserialize)]
pub struct RecoveryCodesResponse {
    pub secret: String,
    pub codes: Vec<String>,
}
