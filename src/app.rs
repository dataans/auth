use std::io;
use std::rc::Rc;
use std::sync::Arc;

use actix_web::HttpServer;
use async_mutex::Mutex;
use clap::Parser;
use deadpool_postgres::tokio_postgres::NoTls;
use deadpool_postgres::Runtime;
use image::storage::StorageUtilsFactory;
use session_manager::SessionService;

use crate::config::Args;
use crate::crypto::CryptoUtils;
use crate::db::email::EmailRepository;
use crate::db::recovery_code::RecoveryCodeRepository;
use crate::db::user::UserRepository;
use crate::logging::setup_logger;
use crate::services::auth::AuthService;
use crate::services::oauth::OAuthService;
use crate::services::user::UserService;

pub struct AppData {
    pub auth_service: Mutex<AuthService>,
    pub crypto_utils: Mutex<CryptoUtils>,
    pub oauth_service: Mutex<OAuthService>,
    pub session_service: Arc<Mutex<SessionService>>,
}

impl AppData {
    pub fn new(args: &Args, w: StorageUtilsFactoryWrapper) -> Self {
        let storage_utils = w.0.build_storage_utils();
        let pool = Arc::new(Mutex::new(
            args.pool_config().create_pool(Some(Runtime::Tokio1), NoTls).unwrap(),
        ));

        let session_service = Arc::new(Mutex::new(
            SessionService::new_from_url_and_key(args.session_redis_url.clone(), args.session_cipher_key).unwrap(),
        ));

        Self {
            auth_service: Mutex::new(AuthService::new(
                UserRepository::new(pool.clone()),
                RecoveryCodeRepository::new(pool.clone()),
                session_service.clone(),
                UserService::new(UserRepository::new(pool.clone()), EmailRepository::new(pool.clone())),
                args.avatars_bucket.clone(),
                storage_utils,
            )),
            crypto_utils: Mutex::new(CryptoUtils::new(args.cipher_key)),
            oauth_service: Mutex::new(OAuthService::new(
                UserRepository::new(pool),
                SessionService::new_from_url_and_key(args.session_redis_url.clone(), args.session_cipher_key).unwrap(),
                args.github_oauth_config(),
                args.avatars_bucket.clone(),
            )),
            session_service,
        }
    }
}

pub struct StorageUtilsFactoryWrapper(pub Box<dyn StorageUtilsFactory + Send>);

impl Clone for StorageUtilsFactoryWrapper {
    fn clone(&self) -> Self {
        Self(self.0.boxed_clone())
    }
}

#[macro_export]
macro_rules! app {
    ($c:ident, $args:expr, $storage_utils_factory: expr) => {{
        use actix_cors::Cors;
        use actix_web::web::Bytes;
        use actix_web::{error, web, App, HttpResponse};
        use session_manager::SessionMiddlewareFactory;
        use $c::api::errors::ApiError;
        use $c::api::handlers::{
            add_email, change_password, default_route, delete_email, github_oauth, google_oauth, health,
            regenerate_2fa_recovery_codes, root_health, set_2fa, set_primary_email, sign_in, sign_up, verify_totp,
        };
        use $c::app::AppData;

        let json_config = web::JsonConfig::default()
            .limit(4096)
            .content_type(|mime| mime == mime::APPLICATION_JSON)
            .error_handler(|err, _req| {
                let details = err.to_string().into();
                error::InternalError::from_response(
                    err,
                    HttpResponse::BadRequest()
                        .body::<Bytes>(
                            serde_json::to_vec(&ApiError {
                                message: "Json error".into(),
                                details: Some(details),
                            })
                            .unwrap()
                            .into(),
                        )
                        .into(),
                )
                .into()
            });

        #[allow(deprecated)]
        App::new()
            .wrap(actix_web::middleware::Logger::default())
            .wrap(
                Cors::default()
                    .expose_any_header()
                    .supports_credentials()
                    .allow_any_header()
                    .allow_any_origin()
                    .allow_any_method(),
            )
            .wrap(SessionMiddlewareFactory::new(Rc::new(Mutex::new(
                SessionService::new_from_url_and_key($args.session_redis_url.clone(), $args.session_cipher_key)
                    .unwrap(),
            ))))
            .default_service(web::route().to(default_route))
            .app_data(json_config)
            .data(AppData::new(&$args, $storage_utils_factory.clone()))
            .service(root_health)
            .service(
                web::scope("/api/v1/auth")
                    .service(health)
                    .service(add_email)
                    .service(delete_email)
                    .service(set_primary_email)
                    .service(change_password)
                    .service(verify_totp)
                    .service(sign_in)
                    .service(sign_up)
                    .service(github_oauth)
                    .service(google_oauth)
                    .service(set_2fa)
                    .service(regenerate_2fa_recovery_codes),
            )
    }};
}

pub async fn start_app(storage_utils_factory: StorageUtilsFactoryWrapper) -> io::Result<()> {
    let args = Args::parse();
    let bind_address = ("0.0.0.0", args.port);

    setup_logger();

    HttpServer::new(move || app!(crate, args, storage_utils_factory))
        .bind(bind_address)?
        .run()
        .await
}
