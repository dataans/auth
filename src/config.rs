use clap::builder::ValueParser;
use clap::Parser;
use url::Url;

use crate::crypto::{CipherKey, CIPHER_KEY_LEN};

const CIPHER_KEY_ENV: &str = "CIPHER_KEY";
const AVATARS_BUCKET_ENV: &str = "AVATARS_BUCKET";

pub const AVATARS_HOST: &str = "https://avatars.dataans.com";

const GITHUB_CLIENT_ID_ENV: &str = "GITHUB_CLIENT_ID";
const GITHUB_CLIENT_SECRET_ENV: &str = "GITHUB_CLIENT_SECRET";
const GITHUB_REDIRECT_URI_ENV: &str = "GITHUB_REDIRECT_URI";

const DB_NAME_ENV: &str = "DB_NAME";
const DB_USER_ENV: &str = "DB_USER";
const DB_PASS_ENV: &str = "DB_PASS";

fn parse_env_encryption_key(env_key: &str) -> Result<CipherKey, clap::Error> {
    hex::decode(env_key)
        .map_err(|err| {
            clap::Error::raw(
                clap::error::ErrorKind::InvalidValue,
                format!("invalid hex value: {:?}", err),
            )
        })?
        .try_into()
        .map_err(|err| {
            clap::Error::raw(
                clap::error::ErrorKind::InvalidValue,
                format!("invalid cipher key len: {:?}", err),
            )
        })
}

#[derive(Parser, Debug, Clone)]
#[command(author, version, about)]
pub struct Args {
    #[arg(
        long,
        required = true,
        default_value = "8080",
        help = "Specify port for the server",
        env = "PORT"
    )]
    pub port: u16,

    #[arg(long, required = true, help = "Specify GCP bucket with profile pictures", env = AVATARS_BUCKET_ENV)]
    pub avatars_bucket: String,

    #[arg(long, required = true, help = format!("Specify encryption key ({} bytes) in hex encoding", CIPHER_KEY_LEN), env = CIPHER_KEY_ENV, value_parser = ValueParser::new(parse_env_encryption_key))]
    pub cipher_key: CipherKey,

    #[arg(long, required = true, help = format!("Specify session encryption key ({} bytes) in hex encoding", CIPHER_KEY_LEN), env = session_manager::CIPHER_KEY_ENV, value_parser = ValueParser::new(parse_env_encryption_key))]
    pub session_cipher_key: CipherKey,
    #[arg(long, required = true, help = "Specify redis url for session store", env = session_manager::REDIS_URL_ENV)]
    pub session_redis_url: Url,

    #[arg(long, help = "Specify main database name", env = DB_NAME_ENV)]
    pub db_name: String,
    #[arg(long, help = "Specify main database user", env = DB_USER_ENV)]
    pub db_user: String,
    #[arg(long, help = "Specify main database user password", env = DB_PASS_ENV)]
    pub db_pass: String,

    #[arg(long, required = true, help = "Specify GitHub client id for OAuth", env = GITHUB_CLIENT_ID_ENV)]
    pub github_client_id: String,
    #[arg(long, required = true, help = "Specify GitHub client secret for OAuth", env = GITHUB_CLIENT_SECRET_ENV)]
    pub github_client_secret: String,
    #[arg(long, required = true, help = "Specify GitHub redirect URI for OAuth", env = GITHUB_REDIRECT_URI_ENV)]
    pub github_redirect_uri: String,
}

impl Args {
    pub fn bind_address(&self) -> (&str, u16) {
        ("0.0.0.0", self.port)
    }

    pub fn pool_config(&self) -> deadpool_postgres::Config {
        let mut config = deadpool_postgres::Config::new();

        config.dbname = Some(self.db_name.clone());
        config.user = Some(self.db_user.clone());
        config.password = Some(self.db_pass.clone());
        config.host = Some("localhost".to_owned());

        config
    }

    pub fn github_oauth_config(&self) -> GitHubOAuthConfig {
        GitHubOAuthConfig {
            id: self.github_client_id.clone(),
            secret: self.github_client_secret.clone(),
            redirect_uri: self.github_redirect_uri.clone(),
        }
    }
}

pub struct GitHubOAuthConfig {
    pub id: String,
    pub secret: String,
    pub redirect_uri: String,
}
