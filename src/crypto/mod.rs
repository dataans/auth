use std::convert::TryInto;

use chacha20poly1305::aead::generic_array::GenericArray;
use chacha20poly1305::aead::Aead;
use chacha20poly1305::{KeyInit, XChaCha20Poly1305, XNonce};
use rand::rngs::ThreadRng;
use rand::Rng;
use sha3::Digest;
use thiserror::Error;

const SALT_BYTES_AMOUNT: usize = 32;

pub const CIPHER_KEY_LEN: usize = 32;
pub type CipherKey = [u8; CIPHER_KEY_LEN];

#[derive(Error, Debug)]
pub enum CryptoError {
    #[error("Argon2 error: {0:?}")]
    Argon2(#[from] argon2::Error),
    #[error("XChaCha20Poly1305 error: {0}")]
    XChaCha20Poly1305(String),
}

pub struct CryptoUtils {
    key: CipherKey,
    rng: ThreadRng,
}

impl CryptoUtils {
    pub fn new(key: CipherKey) -> Self {
        Self {
            key,
            rng: ThreadRng::default(),
        }
    }

    pub fn sha256(data: &[u8]) -> String {
        let mut sha256 = sha3::Sha3_256::new();
        sha256.update(data);
        hex::encode(sha256.finalize())
    }

    pub fn random_bytes<const N: usize>(&mut self) -> [u8; N] {
        (0..N)
            .map(|_| self.rng.gen::<u8>())
            .collect::<Vec<_>>()
            .try_into()
            .unwrap()
    }

    pub fn hash_password(&mut self, password: &str) -> Result<(String, String), CryptoError> {
        let mut sha256 = sha3::Sha3_256::new();
        sha256.update(password);
        let password = sha256.finalize();

        let config = argon2::Config::default();
        let password = argon2::hash_encoded(password.as_slice(), &self.random_bytes::<SALT_BYTES_AMOUNT>(), &config)?;

        let cipher = XChaCha20Poly1305::new(GenericArray::from_slice(&self.key));

        let nonce = self.random_bytes::<24>();
        let cipher_data = cipher
            .encrypt(XNonce::from_slice(&nonce), password.as_bytes())
            .map_err(|e| CryptoError::XChaCha20Poly1305(format!("{:?}", e)))?;

        Ok((hex::encode(nonce), hex::encode(cipher_data)))
    }

    pub fn check_password(&self, password: &str, nonce: &str, hash: &str) -> Result<bool, CryptoError> {
        let mut sha256 = sha3::Sha3_256::new();
        sha256.update(password);
        let password = sha256.finalize();

        let cipher = XChaCha20Poly1305::new(GenericArray::from_slice(&self.key));

        let argon_hash = cipher
            .decrypt(
                XNonce::from_slice(&hex::decode(nonce).unwrap()),
                hex::decode(hash).unwrap().as_slice(),
            )
            .map_err(|e| CryptoError::XChaCha20Poly1305(format!("{:?}", e)))?;

        let argon_hash = String::from_utf8(argon_hash).map_err(|_| CryptoError::Argon2(argon2::Error::DecodingFail))?;

        Ok(argon2::verify_encoded(&argon_hash, password.as_slice())?)
    }

    pub fn generate_2fa_secret(&mut self) -> [u8; 64] {
        (0..64)
            .map(|_| self.rng.gen_range(33..=126))
            .collect::<Vec<_>>()
            .try_into()
            .unwrap()
    }
}
