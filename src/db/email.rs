use std::convert::TryFrom;
use std::sync::Arc;

use async_mutex::Mutex;
use deadpool_postgres::{Object, Pool, PoolError};
use uuid::Uuid;

use super::DbError;
use crate::model::Email;

const ADD_USER_EMAIL: &str = "insert into emails (id, email, user_id, added_at) values ($1, $2, $3, $4)";
const FIND_USER_EMAILS: &str = "select id, email, user_id, added_at from emails where user_id = $1";
const FIND_BY_EMAIL: &str = "select id, email, user_id, added_at from emails where email = $1";
const DELETE_USER_EMAIL_BY_ID: &str = "delete from emails where id = $1";

pub struct EmailRepository {
    pool: Arc<Mutex<Pool>>,
}

impl EmailRepository {
    pub fn new(pool: Arc<Mutex<Pool>>) -> Self {
        Self { pool }
    }

    async fn get_connection(&self) -> Result<Object, PoolError> {
        self.pool.lock().await.get().await
    }

    pub async fn add_email(&self, email: &Email) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_USER_EMAIL).await?;

        client
            .execute(&smt, &[&email.id, &email.email, &email.user_id, &email.added_at])
            .await?;

        Ok(())
    }

    pub async fn find_by_email(&self, email: &str) -> Result<Option<Email>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_EMAIL).await?;

        match client.query(&smt, &[&email]).await?.into_iter().next() {
            Some(row) => Ok(Some(Email::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn find_user_emails(&self, user_id: &Uuid) -> Result<Vec<Email>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_USER_EMAILS).await?;

        let rows = client.query(&smt, &[user_id]).await?;

        let mut emails = Vec::with_capacity(rows.len());
        for raw_user in rows {
            emails.push(Email::try_from(raw_user)?);
        }
        Ok(emails)
    }

    pub async fn delete_email_by_id(&self, id: &Uuid) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(DELETE_USER_EMAIL_BY_ID).await?;

        client.execute(&smt, &[id]).await?;

        Ok(())
    }
}
