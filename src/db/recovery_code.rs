use std::sync::Arc;

use async_mutex::Mutex;
use deadpool_postgres::{Object, Pool, PoolError};
use uuid::Uuid;

use super::DbError;
use crate::model::RecoveryCode;

const ADD_RECOVERY_CODE: &str =
    "insert into _2fa_recovery_codes (id, nonce, hash, version, user_id) values ($1, $2, $3, $4, $5)";
const FIND_USER_RECOVERY_CODES: &str =
    "select id, nonce, hash, version, user_id from _2fa_recovery_codes where user_id = $1";
const DELETE_RECOVERY_CODES: &str = "delete from _2fa_recovery_codes where user_id = $1";

pub struct RecoveryCodeRepository {
    pool: Arc<Mutex<Pool>>,
}

impl RecoveryCodeRepository {
    pub fn new(pool: Arc<Mutex<Pool>>) -> Self {
        Self { pool }
    }

    async fn get_connection(&self) -> Result<Object, PoolError> {
        self.pool.lock().await.get().await
    }

    pub async fn save_recovery_code(&self, code: &RecoveryCode) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_RECOVERY_CODE).await?;

        client
            .execute(&smt, &[&code.id, &code.nonce, &code.hash, &code.version, &code.user_id])
            .await?;

        Ok(())
    }

    pub async fn find_user_recovery_codes(&self, user_id: &Uuid) -> Result<Vec<RecoveryCode>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_USER_RECOVERY_CODES).await?;

        let rows = client.query(&smt, &[user_id]).await?;

        let mut recovery_codes = Vec::with_capacity(rows.len());
        for raw_recovery_code in rows {
            recovery_codes.push(RecoveryCode::try_from(raw_recovery_code)?);
        }
        Ok(recovery_codes)
    }

    pub async fn delete_codes(&self, user_id: &Uuid) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(DELETE_RECOVERY_CODES).await?;

        client.execute(&smt, &[user_id]).await?;

        Ok(())
    }
}
