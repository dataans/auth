use std::convert::TryFrom;
use std::sync::Arc;

use async_mutex::Mutex;
use deadpool_postgres::{Object, Pool, PoolError};
use uuid::Uuid;

use super::DbError;
use crate::model::{Email, Password, User, UserFull};

const ADD_USER: &str =
    "insert into users (id, username, full_name, avatar_url, joined_at, primary_email) values ($1, $2, $3, $4, $5, $6);";
const ADD_PASSWORD: &str = "insert into passwords (id, nonce, hash) values ($1, $2, $3);";
const FIND_PASSWORD: &str = "select id, nonce, hash from passwords where id = $1";
const FIND_BY_USERNAME: &str =
    "select users.id, username, full_name, avatar_url, joined_at, emails.email as primary_email, two_fa_secret, is_2fa_enabled from users left join emails on emails.user_id = users.id where username = $1 and emails.email = users.primary_email";
const FIND_BY_EMAIL: &str =
    "select users.id, username, full_name, avatar_url, joined_at, emails.email as primary_email, two_fa_secret, is_2fa_enabled from users left join emails on emails.user_id = users.id where emails.email = $1";
const FIND_BY_ID: &str =
    "select id, username, full_name, avatar_url, joined_at, primary_email, two_fa_secret, is_2fa_enabled from users where id = $1";
const ADD_USER_EMAIL: &str = "insert into emails (id, email, user_id, added_at) values ($1, $2, $3, $4)";
const SET_PRIMARY_EMAIL: &str = "update users set primary_email = $2 where id = $1";
const UPDATE_PASSWORD: &str = "update passwords set nonce = $2, hash = $3 where id = $1";
const UPDATE_2FA_PARAMETERS: &str = "update users set two_fa_secret = $2, is_2fa_enabled = $3 where id = $1";

pub struct UserRepository {
    pool: Arc<Mutex<Pool>>,
}

impl UserRepository {
    pub fn new(pool: Arc<Mutex<Pool>>) -> Self {
        Self { pool }
    }

    async fn get_connection(&self) -> Result<Object, PoolError> {
        self.pool.lock().await.get().await
    }

    pub async fn find_by_username(&self, username: &str) -> Result<Option<User>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_USERNAME).await?;

        match client.query(&smt, &[&username]).await?.into_iter().next() {
            Some(row) => Ok(Some(User::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn find_by_email(&self, email: &str) -> Result<Option<User>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_EMAIL).await?;

        match client.query(&smt, &[&email]).await?.into_iter().next() {
            Some(row) => Ok(Some(User::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn find_by_id(&self, user_id: &Uuid) -> Result<Option<User>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_ID).await?;

        match client.query(&smt, &[user_id]).await?.into_iter().next() {
            Some(row) => Ok(Some(User::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn find_password(&self, id: &Uuid) -> Result<Option<Password>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_PASSWORD).await?;

        match client.query(&smt, &[id]).await?.into_iter().next() {
            Some(row) => Ok(Some(Password::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn add_with_password_and_email(&self, user_data: &UserFull, email: &Email) -> Result<(), DbError> {
        let mut client = self.get_connection().await?;

        debug!("User creation started.");

        let transaction = client.transaction().await?;

        transaction
            .execute("SET CONSTRAINTS primary_email_in_emails_fkey DEFERRED", &[])
            .await?;

        debug!("update CONSTRAINTS.");

        let smt = transaction.prepare_cached(ADD_USER).await?;
        transaction
            .execute(
                &smt,
                &[
                    &user_data.id,
                    &user_data.username,
                    &user_data.full_name,
                    &user_data.avatar_url,
                    &user_data.joined_at,
                    &email.email,
                ],
            )
            .await?;

        debug!("User added.");

        let smt = transaction.prepare_cached(ADD_USER_EMAIL).await?;
        transaction
            .execute(&smt, &[&email.id, &email.email, &email.user_id, &email.added_at])
            .await?;

        debug!("User email added.");

        let smt = transaction.prepare_cached(ADD_PASSWORD).await?;
        transaction
            .execute(
                &smt,
                &[&user_data.id, &user_data.password.nonce, &user_data.password.hash],
            )
            .await?;

        debug!("Password added.");

        transaction.commit().await?;

        Ok(())
    }

    pub async fn add(&self, user_data: &User) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_USER).await?;

        client
            .query(
                &smt,
                &[
                    &user_data.id,
                    &user_data.username,
                    &user_data.full_name,
                    &user_data.primary_email,
                    &user_data.avatar_url,
                ],
            )
            .await?;

        Ok(())
    }

    pub async fn update_2fa(&self, user: &User) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(UPDATE_2FA_PARAMETERS).await?;

        client
            .query(&smt, &[&user.id, &user.two_fa_secret, &user.is_2fa_enabled])
            .await?;

        Ok(())
    }

    pub async fn update_primary_email(&self, user_id: &Uuid, email: &str) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(SET_PRIMARY_EMAIL).await?;

        client.query(&smt, &[user_id, &email]).await?;

        Ok(())
    }

    pub async fn update_password(&self, password: &Password) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(UPDATE_PASSWORD).await?;

        client
            .query(&smt, &[&password.id, &password.nonce, &password.hash])
            .await?;

        Ok(())
    }
}
