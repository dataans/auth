use auth::app::StorageUtilsFactoryWrapper;
use image::storage::cloud_storage::CloudStorageFactory;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    auth::app::start_app(StorageUtilsFactoryWrapper(Box::<CloudStorageFactory>::default())).await
}
