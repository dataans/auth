use std::convert::TryFrom;

use deadpool_postgres::tokio_postgres::Row;
use time::OffsetDateTime;
use uuid::Uuid;

pub struct User {
    pub id: Uuid,
    pub username: String,
    pub full_name: String,
    pub avatar_url: String,
    pub joined_at: OffsetDateTime,
    pub primary_email: String,
    pub two_fa_secret: Option<String>,
    pub is_2fa_enabled: bool,
}

impl TryFrom<Row> for User {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            id: row.try_get("id")?,
            username: row.try_get("username")?,
            full_name: row.try_get("full_name")?,
            avatar_url: row.try_get("avatar_url")?,
            joined_at: row.try_get("joined_at")?,
            primary_email: row.try_get("primary_email")?,
            two_fa_secret: row.try_get("two_fa_secret")?,
            is_2fa_enabled: row.try_get("is_2fa_enabled")?,
        })
    }
}

pub struct Password {
    pub id: Uuid,
    pub nonce: String,
    pub hash: String,
}

impl TryFrom<Row> for Password {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            id: row.try_get(0)?,
            nonce: row.try_get(1)?,
            hash: row.try_get(2)?,
        })
    }
}

pub struct RecoveryCode {
    pub id: Uuid,
    pub nonce: String,
    pub hash: String,
    pub version: i16,
    pub user_id: Uuid,
}

impl TryFrom<Row> for RecoveryCode {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            id: row.try_get(0)?,
            nonce: row.try_get(1)?,
            hash: row.try_get(2)?,
            version: row.try_get(3)?,
            user_id: row.try_get(4)?,
        })
    }
}

pub struct UserFull {
    pub id: Uuid,
    pub username: String,
    pub full_name: String,
    pub avatar_url: String,
    pub joined_at: OffsetDateTime,
    pub primary_email: String,
    pub password: Password,
}

pub struct Email {
    pub id: Uuid,
    pub email: String,
    pub user_id: Uuid,
    pub added_at: OffsetDateTime,
}

impl TryFrom<Row> for Email {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            id: row.try_get(0)?,
            email: row.try_get(1)?,
            user_id: row.try_get(2)?,
            added_at: row.try_get(3)?,
        })
    }
}
