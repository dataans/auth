use actix_web::body::BoxBody;
use actix_web::error::HttpError;
use actix_web::web::Bytes;
use actix_web::{HttpResponse, ResponseError};
use reqwest::header::{HeaderName, HeaderValue};
use reqwest::StatusCode;
use session_manager::SessionError;
use thiserror::Error;

use crate::api::errors::{ApiError, BAD_CREDENTIALS, INTERNAL, USER_ALREADY_EXIST};
use crate::crypto::CryptoError;
use crate::db::DbError;
use crate::services::user::UserError;

#[derive(Error, Debug)]
pub enum AuthError {
    #[error("Bad credentials")]
    BadCredentials,
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Crypto error: {0:?}")]
    CryptoError(#[from] CryptoError),
    #[error("Session error: {0:?}")]
    Session(#[from] SessionError),
    #[error("Validation error: {0}")]
    Validation(#[from] validator::Error),
    #[error("User already exist")]
    UserAlreadyExist,
    #[error("{0:?}")]
    ImageError(#[from] image::ImageError),
    #[error("User error: {0:?}")]
    User(#[from] UserError),
    #[error("Authentication error: {0}")]
    NotAuthorized(String),
    #[error("Redis error: {0:?}")]
    RedisConnection(#[from] redis::RedisError),
    #[error("2FA error: {0}")]
    TwoFA(String),
    #[error("Internal Actix error: {0:?}")]
    ActixError(#[from] HttpError),
}

impl ResponseError for AuthError {
    fn status_code(&self) -> StatusCode {
        match self {
            AuthError::BadCredentials => StatusCode::UNAUTHORIZED,
            AuthError::NotAuthorized(_) => StatusCode::UNAUTHORIZED,
            AuthError::TwoFA(_) => StatusCode::UNAUTHORIZED,
            AuthError::Validation(_) => StatusCode::BAD_REQUEST,
            AuthError::UserAlreadyExist => StatusCode::BAD_REQUEST,
            AuthError::User(user_error) => user_error.status_code(),
            AuthError::Session(session_error) => session_error.status_code(),
            e => {
                error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }

    fn error_response(&self) -> HttpResponse<BoxBody> {
        let mut res = HttpResponse::new(self.status_code());

        let body: Bytes = match self {
            AuthError::BadCredentials => serde_json::to_vec(&BAD_CREDENTIALS).unwrap().into(),
            AuthError::InternalDbError(_) => serde_json::to_vec(&INTERNAL).unwrap().into(),
            AuthError::CryptoError(_) => serde_json::to_vec(&INTERNAL).unwrap().into(),
            AuthError::Session(error) => return error.error_response(),
            AuthError::Validation(cause) => serde_json::to_vec(&ApiError {
                message: "Invalid data".into(),
                details: Some(cause.to_string().into()),
            })
            .unwrap()
            .into(),
            AuthError::UserAlreadyExist => serde_json::to_vec(&USER_ALREADY_EXIST).unwrap().into(),
            AuthError::ImageError(_) => serde_json::to_vec(&INTERNAL).unwrap().into(),
            AuthError::User(error) => return error.error_response(),
            AuthError::NotAuthorized(message) => serde_json::to_vec(&ApiError {
                message: "Authorization error".into(),
                details: Some(message.into()),
            })
            .unwrap()
            .into(),
            AuthError::RedisConnection(_) => serde_json::to_vec(&INTERNAL).unwrap().into(),
            AuthError::TwoFA(_) => serde_json::to_vec(&ApiError {
                message: "2FA error".into(),
                details: Some(self.to_string().into()),
            })
            .unwrap()
            .into(),
            AuthError::ActixError(_) => serde_json::to_vec(&INTERNAL).unwrap().into(),
        };

        res.head_mut().headers_mut().insert(
            HeaderName::from_static("content-type"),
            HeaderValue::from_static("application/json"),
        );

        res.set_body(BoxBody::new(body))
    }
}
