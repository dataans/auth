mod error;
mod sub_session;
mod token;

use std::env;
use std::sync::Arc;

use async_mutex::Mutex;
pub use error::AuthError;
use image::generate::avatar;
use image::storage::StorageUtils;
use libreauth::oath::TOTPBuilder;
use rand::rngs::ThreadRng;
use session_manager::{decrypt_uuid, encrypt_uuid, SessionService};
use time::{Duration, OffsetDateTime};
pub use token::{Token, TokenType};
use uuid::Uuid;

use self::sub_session::SubSessionManager;
use super::user::{UserError, UserService};
use crate::api::types::{
    AddEmailRequest, ChangePasswordRequest, Enable2FaRequest, RecoveryCodesRequest, RecoveryCodesResponse,
    RemoveEmailRequest, SetPrimaryEmailRequest, SignInRequest, SignUpRequest, SignUpResponse, TotpRequest,
    UserResponse,
};
use crate::config::AVATARS_HOST;
use crate::crypto::CryptoUtils;
use crate::db::recovery_code::RecoveryCodeRepository;
use crate::db::user::UserRepository;
use crate::model::{Email, Password, RecoveryCode, UserFull};
use crate::services::auth::sub_session::SubSession;

pub const DAYS_SESSION_VALID: i64 = 3;

pub const SUB_SESSION_COOKIE_NAME: &str = "SubSessionId";
pub const MINUTES_SUB_SESSION_VALID: i64 = 5;

pub const RECOVERY_CODES_AMOUNT: usize = 10;
pub const RECOVERY_CODE_BYTES_LEN: usize = 16;

pub struct AuthService {
    user_repository: UserRepository,
    recovery_code_repository: RecoveryCodeRepository,
    session_service: Arc<Mutex<SessionService>>,
    user_service: UserService,
    sub_sessions: SubSessionManager,
    rng: ThreadRng,

    avatars_bucket: String,
    storage_utils: Box<dyn StorageUtils>,
}

impl AuthService {
    pub fn new(
        user_repository: UserRepository,
        recovery_code_repository: RecoveryCodeRepository,
        session_service: Arc<Mutex<SessionService>>,
        user_service: UserService,
        avatars_bucket: String,
        storage_utils: Box<dyn StorageUtils>,
    ) -> Self {
        Self {
            user_repository,
            recovery_code_repository,
            session_service,
            user_service,
            sub_sessions: SubSessionManager::new("redis://127.0.0.1:6379"),
            rng: ThreadRng::default(),
            avatars_bucket,
            storage_utils,
        }
    }

    pub async fn sign_in(
        &mut self,
        sign_in_data: &SignInRequest,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<Token, AuthError> {
        let mut user = if sign_in_data.login.find('@').is_some() {
            self.user_repository
                .find_by_email(&sign_in_data.login)
                .await?
                .ok_or(AuthError::BadCredentials)?
        } else {
            self.user_repository
                .find_by_username(&sign_in_data.login)
                .await?
                .ok_or(AuthError::BadCredentials)?
        };

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&sign_in_data.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        if user.is_2fa_enabled {
            if user.two_fa_secret.is_none() {
                user.two_fa_secret = Some(String::from_utf8(crypto_utils.generate_2fa_secret().to_vec()).unwrap());
                self.user_repository.update_2fa(&user).await?;
            }
            // generate and save temporary session
            let sub_session_id = Uuid::new_v4();
            let sub_session = SubSession {
                user_id: user.id,
                expired_at: OffsetDateTime::now_utc()
                    .checked_add(Duration::minutes(MINUTES_SUB_SESSION_VALID))
                    .unwrap(),
            };

            self.sub_sessions
                .save_session(&sub_session_id.to_string(), &sub_session)
                .await?;

            let sub_session_id = encrypt_uuid(
                sub_session_id,
                &mut self.rng,
                &hex::decode(env::var(session_manager::CIPHER_KEY_ENV).unwrap()).unwrap(),
            )?;

            // send temp cookie to the client
            return Ok(Token {
                token_type: TokenType::SubSession,
                token: sub_session_id,
            });
        }

        let session = self
            .session_service
            .lock()
            .await
            .new_session(
                user.id,
                OffsetDateTime::now_utc()
                    .checked_add(Duration::days(DAYS_SESSION_VALID))
                    .unwrap(),
            )
            .await?;
        info!("new session: {}", session);

        Ok(Token {
            token_type: TokenType::Session,
            token: session,
        })
    }

    pub async fn verify_totp(
        &mut self,
        session_id: &str,
        data: TotpRequest,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<Token, AuthError> {
        let session_id = decrypt_uuid(
            session_id,
            &hex::decode(env::var(session_manager::CIPHER_KEY_ENV).unwrap()).unwrap(),
        )?;

        let SubSession { user_id, expired_at } = self.sub_sessions.get_session(&session_id.to_string()).await?;

        if expired_at < OffsetDateTime::now_utc() {
            return Err(AuthError::NotAuthorized("Expired".into()));
        }

        let user = self
            .user_repository
            .find_by_id(&user_id)
            .await?
            .ok_or(AuthError::User(UserError::NotFound(user_id)))?;

        if !user.is_2fa_enabled || user.two_fa_secret.is_none() {
            return Err(AuthError::NotAuthorized("2FA is not enabled for this user".into()));
        }

        let totp = TOTPBuilder::new()
            .ascii_key(user.two_fa_secret.as_ref().unwrap())
            .finalize()
            .unwrap();

        if !totp.is_valid(&data.code) {
            // check if user enter one the recovery codes
            let mut is_recovery_code = false;
            for code in self.recovery_code_repository.find_user_recovery_codes(&user_id).await? {
                if crypto_utils.check_password(&data.code, &code.nonce, &code.hash)? {
                    is_recovery_code = true;
                }
            }

            if !is_recovery_code {
                return Err(AuthError::NotAuthorized("Invalid one time password".into()));
            }
        }

        let session = self
            .session_service
            .lock()
            .await
            .new_session(
                user.id,
                OffsetDateTime::now_utc()
                    .checked_add(Duration::days(DAYS_SESSION_VALID))
                    .unwrap(),
            )
            .await?;
        debug!(session = ?session, "new session");

        Ok(Token {
            token_type: TokenType::Session,
            token: session,
        })
    }

    pub async fn enable_2fa(
        &mut self,
        enable: Enable2FaRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<UserResponse, AuthError> {
        let mut user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::User(UserError::NotFound(*user_id)))?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&enable.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        user.is_2fa_enabled = enable.enable;

        // regenerate 2fa secret every time when user enable or reenable 2fa
        if user.is_2fa_enabled {
            user.two_fa_secret = Some(String::from_utf8(crypto_utils.generate_2fa_secret().to_vec()).unwrap());
        }

        self.user_repository.update_2fa(&user).await?;

        Ok(self.user_service.get_by_id(user_id).await?)
    }

    pub async fn sign_up(
        &self,
        sign_up_data: &SignUpRequest,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<SignUpResponse, AuthError> {
        let SignUpRequest {
            username,
            full_name,
            email,
            password,
        } = sign_up_data.clone();

        validator::user::validate_username(&username)?;
        validator::user::validate_full_name(&full_name)?;
        validator::user::validate_email(&email)?;
        validator::user::validate_password(&password, &validator::user::PasswordParameters::default())?;

        if self.user_repository.find_by_username(&username).await?.is_some()
            || self.user_repository.find_by_email(&email).await?.is_some()
        {
            return Err(AuthError::UserAlreadyExist);
        }

        let avatar_file_name = format!("{}.png", CryptoUtils::sha256(sign_up_data.username.as_bytes()));

        self.storage_utils
            .save_image(avatar(), &self.avatars_bucket, &avatar_file_name)
            .await?;

        let (nonce, hash) = crypto_utils.hash_password(&password)?;
        let id = Uuid::new_v4();

        let user = UserFull {
            id,
            username,
            full_name,
            password: Password { id, hash, nonce },
            avatar_url: format!("{}/{}", AVATARS_HOST, avatar_file_name),
            joined_at: OffsetDateTime::now_utc(),
            primary_email: email.clone(),
        };

        let email = Email {
            id: Uuid::new_v4(),
            email,
            user_id: user.id,
            added_at: OffsetDateTime::now_utc(),
        };

        self.user_repository.add_with_password_and_email(&user, &email).await?;

        info!("User with username {} registered", user.username);

        Ok(SignUpResponse { user_id: id })
    }

    pub async fn change_password(
        &mut self,
        passwords: ChangePasswordRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<(), AuthError> {
        let ChangePasswordRequest {
            password: old_password,
            new_password,
        } = passwords;

        let user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&old_password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        validator::user::validate_password(&new_password, &validator::user::PasswordParameters::default())?;

        let (nonce, hash) = crypto_utils.hash_password(&new_password)?;
        let new_password = Password {
            id: *user_id,
            hash,
            nonce,
        };

        self.user_repository.update_password(&new_password).await?;

        Ok(())
    }

    pub async fn add_user_email(
        &mut self,
        email: AddEmailRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<UserResponse, AuthError> {
        let user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&email.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        validator::user::validate_email(&email.email)?;

        Ok(self.user_service.add_email(&email.email, user_id).await?)
    }

    pub async fn delete_user_email(
        &mut self,
        email: RemoveEmailRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<UserResponse, AuthError> {
        let user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&email.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        Ok(self.user_service.delete_email(&email.email, user_id).await?)
    }

    pub async fn set_user_primary_email(
        &mut self,
        email: SetPrimaryEmailRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<UserResponse, AuthError> {
        let user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&email.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        Ok(self.user_service.set_primary_email(&email.email, user_id).await?)
    }

    pub async fn regenerate_2fa_recovery_codes(
        &mut self,
        data: RecoveryCodesRequest,
        user_id: &Uuid,
        crypto_utils: &mut CryptoUtils,
    ) -> Result<RecoveryCodesResponse, AuthError> {
        let mut user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        let password = self
            .user_repository
            .find_password(&user.id)
            .await?
            .ok_or(AuthError::BadCredentials)?;

        if !crypto_utils.check_password(&data.password, &password.nonce, &password.hash)? {
            return Err(AuthError::BadCredentials);
        }

        if !user.is_2fa_enabled {
            return Err(AuthError::TwoFA("is not enabled".into()));
        }

        if user.is_2fa_enabled && user.two_fa_secret.is_none() {
            user.two_fa_secret = Some(String::from_utf8(crypto_utils.generate_2fa_secret().to_vec()).unwrap());
        }

        self.recovery_code_repository.delete_codes(user_id).await?;

        let mut codes = Vec::with_capacity(RECOVERY_CODES_AMOUNT);
        for _ in 0..RECOVERY_CODES_AMOUNT {
            let code = hex::encode(crypto_utils.random_bytes::<RECOVERY_CODE_BYTES_LEN>());

            let (nonce, hash) = crypto_utils.hash_password(&code)?;
            let recovery_code = RecoveryCode {
                id: Uuid::new_v4(),
                nonce,
                hash,
                version: 1,
                user_id: *user_id,
            };
            self.recovery_code_repository.save_recovery_code(&recovery_code).await?;

            codes.push(code);
        }

        Ok(RecoveryCodesResponse {
            codes,
            secret: user.two_fa_secret.unwrap(),
        })
    }
}
