use redis::{AsyncCommands, Client};
use serde::{Deserialize, Serialize};
use serde_json::{from_str, to_string};
use time::OffsetDateTime;
use uuid::Uuid;

use super::AuthError;

#[derive(Serialize, Deserialize, Clone)]
pub struct SubSession {
    pub user_id: Uuid,
    pub expired_at: OffsetDateTime,
}

pub struct SubSessionManager {
    redis: Client,
}

impl SubSessionManager {
    pub fn new(redis_url: &str) -> Self {
        SubSessionManager {
            redis: Client::open(redis_url).unwrap(),
        }
    }

    pub async fn save_session(&mut self, session_id: &str, session: &SubSession) -> Result<(), AuthError> {
        let mut connection = self.redis.get_async_connection().await?;

        connection
            .set::<&str, String, ()>(session_id, to_string(&session).unwrap())
            .await?;

        Ok(())
    }

    pub async fn get_session(&mut self, session_id: &str) -> Result<SubSession, AuthError> {
        let mut connection = self.redis.get_async_connection().await?;

        let session_json: String = connection.get(session_id).await?;

        Ok(from_str(&session_json).unwrap())
    }
}
