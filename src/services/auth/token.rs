use std::borrow::Cow;

use session_manager::AUTH_COOKIE_NAME;

use super::SUB_SESSION_COOKIE_NAME;

pub enum TokenType {
    Session,
    SubSession,
}

impl<'a> From<TokenType> for Cow<'a, str> {
    fn from(token_type: TokenType) -> Self {
        match token_type {
            TokenType::Session => Cow::Borrowed(AUTH_COOKIE_NAME),
            TokenType::SubSession => Cow::Borrowed(SUB_SESSION_COOKIE_NAME),
        }
    }
}

pub struct Token {
    pub token_type: TokenType,
    pub token: String,
}
