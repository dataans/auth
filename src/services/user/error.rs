use actix_web::body::BoxBody;
use actix_web::http::StatusCode;
use actix_web::web::Bytes;
use actix_web::{HttpResponse, ResponseError};
use reqwest::header::{HeaderName, HeaderValue};
use session_manager::SessionError;
use thiserror::Error;
use uuid::Uuid;

use crate::api::errors::{ApiError, BAD_CREDENTIALS, INTERNAL};
use crate::crypto::CryptoError;
use crate::db::DbError;

#[derive(Error, Debug)]
pub enum UserError {
    #[error("Bad credentials")]
    BadCredentials,
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Crypto error: {0:?}")]
    CryptoError(#[from] CryptoError),
    #[error("Session error: {0:?}")]
    Session(#[from] SessionError),
    #[error("Validation error: {0}")]
    Validation(#[from] validator::Error),
    #[error("Email {0} already used by someone")]
    AlreadyUsed(String),
    #[error("User with id {0:?} not found")]
    NotFound(Uuid),
    #[error("Email {0:?} not found")]
    EmailNotFound(String),
    #[error("Permission denied: {0}")]
    PermissionDenied(String),
    #[error("Email deletion error: {0}")]
    EmailDeletion(String),
}

impl ResponseError for UserError {
    fn status_code(&self) -> StatusCode {
        match self {
            UserError::BadCredentials => StatusCode::UNAUTHORIZED,
            UserError::Validation(_) => StatusCode::BAD_REQUEST,
            UserError::AlreadyUsed(_) => StatusCode::BAD_REQUEST,
            UserError::EmailDeletion(_) => StatusCode::BAD_REQUEST,
            UserError::NotFound(_) => StatusCode::NOT_FOUND,
            UserError::EmailNotFound(_) => StatusCode::NOT_FOUND,
            UserError::PermissionDenied(_) => StatusCode::FORBIDDEN,
            UserError::Session(session_error) => session_error.status_code(),
            e => {
                error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }

    fn error_response(&self) -> HttpResponse<BoxBody> {
        let mut res = HttpResponse::new(self.status_code());

        let body: Bytes = match self {
            UserError::BadCredentials => serde_json::to_vec(&BAD_CREDENTIALS).unwrap().into(),
            UserError::InternalDbError(_) => serde_json::to_vec(&INTERNAL).unwrap().into(),
            UserError::CryptoError(_) => serde_json::to_vec(&INTERNAL).unwrap().into(),
            UserError::Session(error) => return error.error_response(),
            UserError::Validation(cause) => serde_json::to_vec(&ApiError {
                message: "Invalid data".into(),
                details: Some(cause.to_string().into()),
            })
            .unwrap()
            .into(),
            UserError::AlreadyUsed(_) => serde_json::to_vec(&ApiError {
                message: self.to_string().into(),
                details: None,
            })
            .unwrap()
            .into(),
            UserError::NotFound(_) => serde_json::to_vec(&ApiError {
                message: self.to_string().into(),
                details: None,
            })
            .unwrap()
            .into(),
            UserError::EmailNotFound(_) => serde_json::to_vec(&ApiError {
                message: self.to_string().into(),
                details: None,
            })
            .unwrap()
            .into(),
            UserError::PermissionDenied(_) => serde_json::to_vec(&ApiError {
                message: self.to_string().into(),
                details: None,
            })
            .unwrap()
            .into(),
            UserError::EmailDeletion(_) => serde_json::to_vec(&ApiError {
                message: self.to_string().into(),
                details: None,
            })
            .unwrap()
            .into(),
        };

        res.head_mut().headers_mut().insert(
            HeaderName::from_static("content-type"),
            HeaderValue::from_static("application/json"),
        );

        res.set_body(BoxBody::new(body))
    }
}
