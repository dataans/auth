mod utils;

use actix_web::http::StatusCode;
use actix_web::test;
use auth::app::StorageUtilsFactoryWrapper;
use auth::config::Args;
use auth::logging::setup_logger;
use clap::Parser;
use image::storage::mock_storage::MockStorageFactory;
use utils::generate_simple_sign_up_request;

#[macro_use]
extern crate auth;

#[actix_web::test]
async fn basic_sign_up() {
    let args = Args::parse();

    setup_logger();

    let app = test::init_service(app!(
        auth,
        args,
        StorageUtilsFactoryWrapper(Box::new(MockStorageFactory::new("basic_sign_up")))
    ))
    .await;

    let sign_up_request = generate_simple_sign_up_request();

    let request = test::TestRequest::post()
        .uri("/api/v1/auth/sign-up")
        .set_json(sign_up_request)
        .to_request();

    let response = test::call_service(&app, request).await;

    assert!(response.status() == StatusCode::CREATED);
}
