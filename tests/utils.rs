use auth::api::types::SignUpRequest;
use rand::distributions::Alphanumeric;
use rand::seq::SliceRandom;
use rand::{thread_rng, Rng};

#[allow(dead_code)]
pub fn generate_string<const LEN: usize>() -> String {
    let mut rand = thread_rng();

    (0..LEN)
        .into_iter()
        .map(|_| rand.sample(Alphanumeric) as char)
        .collect::<String>()
}

pub fn generate_name<const LEN: usize>() -> String {
    let mut rand = thread_rng();

    (0..LEN)
        .into_iter()
        .map(|_| rand.gen_range(b'a'..b'z') as char)
        .collect::<String>()
}

pub fn generate_password<const LEN: usize>() -> String {
    let chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?";
    let mut rand = thread_rng();

    (0..LEN)
        .into_iter()
        .map(|_| *chars.as_bytes().choose(&mut rand).unwrap() as char)
        .collect::<String>()
}

pub fn generate_simple_sign_up_request() -> SignUpRequest {
    let rand_prefix = generate_name::<16>();
    let username = format!("tu{rand_prefix}");

    let password = generate_password::<16>();

    SignUpRequest {
        username: username.clone(),
        email: format!("{username}@testdataans.com"),
        full_name: format!("{} {}", username, generate_name::<5>()),
        password,
    }
}
